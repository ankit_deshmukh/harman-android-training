package com.brownie.new_application;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String status = String.valueOf(NetworkUtil.getConnectivityStatus(context));

        Toast.makeText(context, "Status = " + status, Toast.LENGTH_LONG).show();
    }
}
