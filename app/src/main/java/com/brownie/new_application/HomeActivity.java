package com.brownie.new_application;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    EditText textNumber, textWord, textMsg;
    Button callbtn, searchbtn, sendmsg;

    private static final int REQUEST_PHONE_CALL = 1;

    private boolean isPhoneGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textNumber = findViewById(R.id.et_phone);
        textWord = findViewById(R.id.et_search);
        callbtn = findViewById(R.id.btn_call);
        searchbtn = findViewById(R.id.btn_make_search);

        callbtn.setOnClickListener(this);
        searchbtn.setOnClickListener(this);

        checkPermissions();
    }

    private void checkPermissions()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_PHONE_CALL :
            {
                Log.d("Home Activity", "onRequestPermissionsResult: call pressed!");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Home", "onRequestPermissionsResult: here");
                    isPhoneGranted = true;

                }
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_call:
            {
                Log.d("Home Activity", "onClick: call btn clicked!");

                    String number = textNumber.getText().toString();
                    Log.d("Home vala", "onClick: number = " + number);

                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+number));
                    startActivity(intent);


            }
            break;

            case R.id.btn_make_search:
            {
                String uri = "http://www.google.com/#q=";
                String query = textWord.getText().toString();
                String final_url = uri + query;

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(final_url));
                startActivity(intent);
            }
            break;
        }
    }
}
