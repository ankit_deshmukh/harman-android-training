package com.brownie.new_application;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brownie.new_application.service.MyService;

import static com.brownie.new_application.App.CHANNEL_ID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button loginbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init()
    {
        loginbtn = findViewById(R.id.btn_start_service);

        loginbtn.setOnClickListener(this);
    }

    private void startService()
    {
                Toast.makeText(this,"Welcome To Harman", Toast.LENGTH_LONG);
                startService(new Intent(this, MyService.class));
    }

    private void stopService()
    {
        stopService(new Intent(this, MyService.class));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_start_service:
            {
                startService();
            }
            break;

            case R.id.btn_stop_service:
            {
                stopService();
            }
        }


    }

    private void onNotify()
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("Demo Notification");
        bigTextStyle.bigText("I have no idea what I'm doing here");

        builder.setStyle(bigTextStyle)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher);

        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setLargeIcon(largeIconBitmap);
    }

}
