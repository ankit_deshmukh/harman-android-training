package com.brownie.new_application;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AsyncTaskDemo extends AppCompatActivity {

    private EditText time;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_demo);

        time = findViewById(R.id.in_time);
        result = findViewById(R.id.thread_result);
        Button asyncBtn = findViewById(R.id.btn_time_in);
        asyncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTaskInnerClass asyncTaskInnerClass = new AsyncTaskInnerClass();
                String val = time.getText().toString();

                asyncTaskInnerClass.execute(val);
            }
        });
    }

    public class AsyncTaskInnerClass extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        private String response;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(AsyncTaskDemo.this, "Progress Dialog","Wait For " + time.getText().toString() + " seconds");
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();

            result.setText("Nikal Laude, pehli fursat mei nikal, Koi zaroorat nahi hindustan ko teri" +
                    "Pehli fursat mei Bhosdike Nikal!!!!!!!!!!!" );
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                int time = Integer.parseInt(strings[0])*1000;
                Thread.sleep(time);
                response = "Slept for " + strings[0] + " seconds";
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return response;
        }
    }
}

