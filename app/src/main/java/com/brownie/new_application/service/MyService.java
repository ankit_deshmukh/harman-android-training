package com.brownie.new_application.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import com.brownie.new_application.R;


public class MyService extends Service {

    private static final String TAG = "MyService";

    MediaPlayer mediaPlayer;

    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: Service created");
        mediaPlayer = MediaPlayer.create(this, R.raw.sun);
        mediaPlayer.setLooping(false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mediaPlayer.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: Service stop");
        mediaPlayer.stop();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
